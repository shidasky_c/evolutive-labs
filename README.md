# interview-ts

## Project setup
```
# Install dependencies
yarn install

# Compiles and hot-reloads for development
yarn serve

# Run mock API server
yarn serve:db
```

## 規則
* 作答時間為 2 小時
* 繳交方式：收到題目 3 天內，將 repository URL 寄回
* 針對題目若有任何疑問，也可以來信詢問

## 題目
clone 此專案，並以新建立的 branch 進行作答。請參考[設計稿](https://www.figma.com/file/Gvsqct4CKMYsHIrjTOT5FA/For-interview?node-id=0%3A1)與[互動原型](https://www.figma.com/proto/Gvsqct4CKMYsHIrjTOT5FA/For-interview)來實作以下功能：

1. 透過 local API server 取得資料（執行 `yarn serve:db` ）
2. 使用者可以透過下拉式選單切換**裝置（device）**
3. 使用者可以選擇 **邊框樣式（case color）** 與 **背板樣式（backplate style）**
4. 使用者可以選擇以 **背板模式（standard）** 或 **邊框模式（bumper）** 進行預覽
5. 點擊加入購物車時，將使用者所選擇的產品資料存入 `cartItems` ，並使用 `console.log` 印出 `cartItems` 內容

## 想法
1. 建立 request, api 套件，以使用 Axios 取得 Mock Data。
2. 產品資料以裝置進行分類。
3. 進行操作開發。

## 問題
3. 將裝置(device)的資料綁定至下拉選單。由於未於專案中，找到圖檔來源。因此，未能更換。但有嘗試將裝置換顏色。
4. 邊框樣式與背板樣式，由於按鈕未提供，僅有顯示字體，故也未進行實作。
5. 背板模式與邊框模式，由於UI尚未完整，故也未進行實作。
6. 因以上需花大量時間處理UI設計，若能具備UI完善，則可將資料綁定後，選擇並加入購物車 cartItems