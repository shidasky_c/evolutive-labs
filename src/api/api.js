import request from '../utils/request'

export const fetchData = () =>
  request({
    method: 'get',
    url: '/mod-nx'
  })
