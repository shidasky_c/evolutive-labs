import axios from 'axios'

// create an axios instance
const service = axios.create({
  baseURL: 'http://localhost:3000/',
  withCredentials: false,
  timeout: 5 * 1000
})

// request interceptor
service.interceptors.request.use(
  config => {
    return config
  },
  error => {
    console.error(`error: ${error}`)
    return Promise.reject(error)
  }
)

// response interceptor
service.interceptors.response.use(
  response => {
    return response.data.data
  },
  error => {
    console.error(`error: ${error}`)
    return Promise.reject(error)
  }
)

export default service
